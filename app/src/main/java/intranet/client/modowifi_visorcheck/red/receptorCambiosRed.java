package intranet.client.modowifi_visorcheck.red;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import intranet.client.modowifi_visorcheck.servicios.TTS;
import intranet.client.modowifi_visorcheck.utilidades.Util;
import intranet.client.modowifi_visorcheck.utilidades.UtilidadesRed;

public class receptorCambiosRed extends BroadcastReceiver {
    private SharedPreferences prefs;
    private boolean status;
    private boolean estaEnHorario = true;
    private String[] horario={"09:00 a 19:30","9:00","19:30","22:00","20"};
    private String[] macs={"14:91:82:29:1d:f1","c0:56:27:95:bb:87","c6:4a:00:27:3a:bd","d4:40:f0:2b:94:14"};
    @Override
    public void onReceive(Context context, Intent intent) {
        if (estaEnHorario) {
            prefs=context.getSharedPreferences("pruebas_modoWifi",Context.MODE_PRIVATE);
            status=Util.obtenerPreferenciaCondicion(prefs,"status",false);
            //crear notificacion running in background
            Log.e(null, "Cambio de red");
            if (UtilidadesRed.wifi_habilitado(context)) {
                if(UtilidadesRed.obtenerTipoConexion(context)==1){
                    if (comprobarConexion(context)){
                        if(status){
                            Util.guardarPreferenciaCondicion(prefs,"status",false);
                            context.startService(new Intent(context, TTS.class));
                        }else{
                            Util.guardarPreferenciaCondicion(prefs,"status",true);
                            context.startService(new Intent(context, TTS.class));
                        }
                    }
                } else{
                    if (status) {//entrada
                        Util.guardarPreferenciaCondicion(prefs,"status",false);
                        context.startService(new Intent(context, TTS.class));
                    }
                }
            } else {
                //enviar un push notification
                if (status) {//entrada
                    Util.guardarPreferenciaCondicion(prefs,"status",false);
                    context.startService(new Intent(context, TTS.class));
                }
            }
        } else {
            //eliminar notificacion running in background
        }
    }
    private boolean comprobarConexion(Context context) {
        boolean coinciden=false;
        String macAP = UtilidadesRed.obtenerMAC_AP(context);
        for (int i = 0; i < macs.length; i++) {
            if (macAP.equals(macs[i])) {
                coinciden=true;
                break;
            }
        }
        return coinciden;
    }
}

