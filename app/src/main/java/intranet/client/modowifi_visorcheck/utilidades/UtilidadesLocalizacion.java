package intranet.client.modowifi_visorcheck.utilidades;

import android.location.Location;

public class UtilidadesLocalizacion {
    public static Location obtenerUbicacionActual(Location location){
        Location ubicacionActual=new Location("puntoA");
        if(location!=null){
            ubicacionActual.setLatitude(location.getLatitude());
            ubicacionActual.setLongitude(location.getLongitude());
            ubicacionActual.setAccuracy(location.getAccuracy());
        }
        return ubicacionActual;
    }
}
