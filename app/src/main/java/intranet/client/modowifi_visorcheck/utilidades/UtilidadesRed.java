package intranet.client.modowifi_visorcheck.utilidades;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Inet6Address;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

public class UtilidadesRed {
    public static int TIPO_WIFI=1;
    public static int TIPO_DATOS_MOVILES=2;
    public static int NO_CONECTADO=0;

    public static int obtenerTipoConexion(Context contex) {
        ConnectivityManager cm = (ConnectivityManager) contex
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infoRed = cm.getActiveNetworkInfo();

        if (infoRed != null) {
            if (infoRed.getType() == ConnectivityManager.TYPE_WIFI)
                return TIPO_WIFI;
            if (infoRed.getType() == ConnectivityManager.TYPE_MOBILE)
                return TIPO_DATOS_MOVILES;
        }
        return NO_CONECTADO;
    }

    public static String obtenerMAC_AP(Context context){
        if(obtenerTipoConexion(context)==1){
            WifiManager manager=(WifiManager)context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo infoWifi=manager.getConnectionInfo();
            String mac= infoWifi.getBSSID();
            return mac;
        }
        return "NO HAY RED WIFI";
    }
    public static String obtenerNombre_wifi(Context context){
        ConnectivityManager cm=(ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo infoRed=cm.getActiveNetworkInfo();

        if(obtenerTipoConexion(context)==1){
            if(infoRed !=null){
                return infoRed.getExtraInfo();
            }else{
                return "No esta conectado a una red";
            }
        }
        return "NO HAY RED WIFI";
    }

    private boolean redDisponible(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo actNetInfo = connectivityManager.getActiveNetworkInfo();

        return (actNetInfo != null && actNetInfo.isConnected());
    }

    public static boolean estaEnLinea(){
        try{
            Process p= Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int val= p.waitFor();
            boolean internet = (val == 0);
            return internet;
        } catch (IOException e) {
            Log.d(null, "________________________________________No esta conectado:"+e);
        } catch (InterruptedException e) {
            Log.d(null, "________________________________________error waitFor:"+e);
        }
        return false;
    }

    public static boolean wifi_habilitado(Context context){
        WifiManager wifiManager=(WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        if((wifiManager.isWifiEnabled()==false)){
            return  false;
        }else{
            return true;
        }
    }

    public static void encenderWifi(Context context){
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";
    }

}
