package intranet.client.modowifi_visorcheck.utilidades;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

/**
 * Created by pc on 10/07/2018.
 */

public class UtilidadesTextToSpeech{
    private TextToSpeech tts;
    public void accesoCorrecto(Context context){
        tts=new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                tts.setLanguage(Locale.US);
                tts.speak("ACCESO CORRECTO",TextToSpeech.QUEUE_ADD,null);
            }
        });
    }
    public void hastaLuego(Context context){
        tts=new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                tts.setLanguage(Locale.US);
                tts.speak("HASTA LUEGO",TextToSpeech.QUEUE_ADD,null);
            }
        });
    }
}
