package intranet.client.modowifi_visorcheck.utilidades;

import android.content.SharedPreferences;
import android.util.Log;

/**
 *Metodos repetitivos. Lista:
 * Obtencion de las preferencias
 */

public class Util {
    public static String obtenerPreferenciaCadena(SharedPreferences prefs, String clave, String valorDefault){ return prefs.getString(clave,valorDefault); }
    public static Boolean obtenerPreferenciaCondicion(SharedPreferences prefs, String clave, Boolean valorDefault){ return prefs.getBoolean(clave,valorDefault); }
    public static void guardarPreferenciaCadena(SharedPreferences prefs,String clave,String valor){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(clave, valor);
        //editor.apply();
        editor.commit();
    }
    public static void guardarPreferenciaCondicion(SharedPreferences prefs,String clave,Boolean valor){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(clave, valor);
        //editor.apply();
        editor.commit();
    }
    public static void guardarPreferenciaLong(SharedPreferences prefs,String clave,long valor){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(clave, valor);
        //editor.apply();
        editor.commit();
    }


    public static void eliminarPreferencias(SharedPreferences prefs){
            prefs.edit().clear().commit();
            Log.e(null,"_________________________Share preferences eliminadas");
    }
}
