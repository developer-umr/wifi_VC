package intranet.client.modowifi_visorcheck.aplicaciones;

import android.app.Application;

import java.util.concurrent.atomic.AtomicInteger;

import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Empleados;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Empresas;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Geocercas;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Horarios;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Logs;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

/***
 *Clase que se ejecuta al iniciar la aapp
 * Se utiliza para realizar la configuracion y creacion de la base de datos con REALM
 */

public class Aplicacion extends Application {
    public static AtomicInteger EmpresaId = new AtomicInteger();
    public static AtomicInteger HorarioId = new AtomicInteger();
    public static AtomicInteger GeocercaId =new AtomicInteger();
    public static AtomicInteger EmpleadoId=new AtomicInteger();
    public static AtomicInteger LogId=new AtomicInteger();

    @Override
    public void onCreate() {
        super.onCreate();
        colocarConfiguracionRealm();
        Realm realm = Realm.getDefaultInstance();
        EmpresaId=setAtomicId(realm, T_Empresas.class);
        HorarioId=setAtomicId(realm, T_Horarios.class);
        GeocercaId=setAtomicId(realm, T_Geocercas.class);
        EmpleadoId=setAtomicId(realm, T_Empleados.class);
        LogId=setAtomicId(realm, T_Logs.class);
        realm.close();
    }

    private void colocarConfiguracionRealm() {
        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder().name("visorCheck.realm").deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
    }

    private <T extends  RealmObject> AtomicInteger setAtomicId(Realm realm, Class<T> anyClass) {
        RealmResults<T> results = realm.where(anyClass).findAll();
        return (results.size() > 0) ? new AtomicInteger(results.max("id").intValue()) : new AtomicInteger();
    }
}
