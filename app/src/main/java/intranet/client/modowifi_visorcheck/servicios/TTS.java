package intranet.client.modowifi_visorcheck.servicios;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;

import java.util.Locale;

import intranet.client.modowifi_visorcheck.utilidades.Util;

public class TTS extends Service implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener{
    private TextToSpeech myTts;
    private String texto;
    private boolean status;
    private SharedPreferences prefs;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        prefs=getSharedPreferences("pruebas_modoWifi", Context.MODE_PRIVATE);
        myTts=new TextToSpeech(this,this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(myTts!=null){
            myTts.stop();
            myTts.shutdown();
        }
    }

    @Override
    public void onInit(int i) {
        if(i==TextToSpeech.SUCCESS){
            int resultado=myTts.setLanguage(Locale.US);
            status= Util.obtenerPreferenciaCondicion(prefs,"status",false);
            if(resultado!=TextToSpeech.LANG_MISSING_DATA && resultado!= TextToSpeech.LANG_NOT_SUPPORTED){
                if(status){
                    myTts.speak("HASTA LUEGO",TextToSpeech.QUEUE_FLUSH,null);
                } else{
                    myTts.speak("ACCESO CORRECTO",TextToSpeech.QUEUE_FLUSH,null);
                }
            }
        }
    }

    @Override
    public void onUtteranceCompleted(String s) {
        stopSelf();
    }
}
