package intranet.client.modowifi_visorcheck.modelo.modelosBD;

import intranet.client.modowifi_visorcheck.aplicaciones.Aplicacion;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by pc on 07/07/2018.
 */

public class T_Empresas extends RealmObject{
    @PrimaryKey
    private long id;
    private long id_empresa; //funge como id devuelta de la base de datos
    private String clave;
    private String razon_social;
    private String rfc;

    public T_Empresas() {
    }

    public T_Empresas(long id_empresa, String clave, String rfc, String razon_social) {
        id= Aplicacion.EmpresaId.incrementAndGet();
        this.id_empresa = id_empresa;
        this.clave = clave;
        this.rfc = rfc;
        this.razon_social = razon_social;
    }

    public void setId_empresa(long id_empresa) {
        this.id_empresa = id_empresa;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public long getId() {
        return id;
    }

    public long getId_empresa() {
        return id_empresa;
    }

    public String getClave() {
        return clave;
    }

    public String getRfc() {
        return rfc;
    }

    public String getRazon_social() {
        return razon_social;
    }
}
