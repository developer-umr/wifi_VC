package intranet.client.modowifi_visorcheck.modelo.modelosBD;

import intranet.client.modowifi_visorcheck.aplicaciones.Aplicacion;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by pc on 07/07/2018.
 */

public class T_Logs extends RealmObject {
    @PrimaryKey
    private long id;
    private long id_log; //funge como el id de la base de datos
    private RealmList<T_Empleados> t_empleado;
    private RealmList<T_Geocercas> t_geocerca;
    private String imagen;
    private String latitud;
    private String longitud;
    private String registro; //fecha + hora
    private boolean status;
    private int tipo;

    public T_Logs() {
    }

    public T_Logs(long id_log, String imagen, String latitud, String longitud, String registro, boolean status, int tipo) {
        this.id_log = id_log;
        this.imagen = imagen;
        this.latitud = latitud;
        this.longitud = longitud;
        this.registro = registro;
        this.status = status;
        this.tipo = tipo;
        id= Aplicacion.LogId.incrementAndGet();
        t_empleado=new RealmList<T_Empleados>();
        t_geocerca=new RealmList<T_Geocercas>();
    }

    public void setId_log(long id_log) {
        this.id_log = id_log;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public long getId() {
        return id;
    }

    public long getId_log() {
        return id_log;
    }

    public RealmList<T_Empleados> getEmpleado() {
        return t_empleado;
    }

    public RealmList<T_Geocercas> getGeocerca() {
        return t_geocerca;
    }

    public String getImagen() {
        return imagen;
    }

    public String getLatitud() {
        return latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public String getRegistro() {
        return registro;
    }

    public boolean isStatus() {
        return status;
    }

    public int getTipo() {
        return tipo;
    }
}
