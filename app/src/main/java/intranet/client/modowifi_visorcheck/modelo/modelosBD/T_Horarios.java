package intranet.client.modowifi_visorcheck.modelo.modelosBD;

import intranet.client.modowifi_visorcheck.aplicaciones.Aplicacion;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by pc on 07/07/2018.
 */

public class T_Horarios extends RealmObject {
    @PrimaryKey
    private long id;
    private long id_horario; //funge como el id recuperado de la bd
    private long DT_RowId;
    private String descripcion;
    private String entrada;
    private String salida;
    private String maxSalida;
    private String minRetraso;
    private boolean activo;
    private int tipo;

    public T_Horarios() {
    }

    public T_Horarios(long id_horario, long DT_RowId, String descripcion, String entrada, String salida, String maxSalida, String minRetraso, boolean activo, int tipo) {
        id = Aplicacion.HorarioId.incrementAndGet();
        this.id_horario = id_horario;
        this.DT_RowId = DT_RowId;
        this.descripcion = descripcion;
        this.entrada = entrada;
        this.salida = salida;
        this.maxSalida = maxSalida;
        this.minRetraso = minRetraso;
        this.activo = activo;
        this.tipo = tipo;
    }

    public void setId_horario(long id_horario) {
        this.id_horario = id_horario;
    }

    public void setDT_RowId(long DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public void setMaxSalida(String maxSalida) {
        this.maxSalida = maxSalida;
    }

    public void setMinRetraso(String minRetraso) {
        this.minRetraso = minRetraso;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public long getId() {
        return id;
    }

    public long getId_horario() {
        return id_horario;
    }

    public long getDT_RowId() {
        return DT_RowId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getEntrada() {
        return entrada;
    }

    public String getSalida() {
        return salida;
    }

    public String getMaxSalida() {
        return maxSalida;
    }

    public String getMinRetraso() {
        return minRetraso;
    }

    public int getTipo() {
        return tipo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
