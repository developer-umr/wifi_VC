package intranet.client.modowifi_visorcheck.modelo.modelosBD;

import intranet.client.modowifi_visorcheck.aplicaciones.Aplicacion;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by pc on 07/07/2018.
 */

public class T_Empleados extends RealmObject{
    @PrimaryKey
    private long id;
    private long id_empleado;
    private long DT_RowId; //funge como id devulta de la base de datos
    private String clave;
    private String uuid;
    private String nombre;
    private RealmList<T_Empresas> t_empresa;
    private boolean status;
    private RealmList<T_Horarios> t_horario;
    private int tipo;

    public T_Empleados() {
    }

    public T_Empleados(long id_empleado, long DT_RowId, String clave, String uuid, String nombre, boolean status, int tipo) {
        this.id_empleado = id_empleado;
        this.DT_RowId = DT_RowId;
        this.clave = clave;
        this.uuid = uuid;
        this.nombre = nombre;
        this.status = status;
        this.tipo=tipo;
        id= Aplicacion.EmpleadoId.incrementAndGet();
        t_empresa=new RealmList<T_Empresas>();
        t_horario=new RealmList<T_Horarios>();
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public long getId() {
        return id;
    }

    public long getId_empleado() {
        return id_empleado;
    }

    public long getDT_RowId() {
        return DT_RowId;
    }

    public String getClave() {
        return clave;
    }

    public String getUuid() {
        return uuid;
    }

    public String getNombre() {
        return nombre;
    }

    public RealmList<T_Empresas> getT_Empresa() {
        return t_empresa;
    }

    public boolean isStatus() {
        return status;
    }

    public RealmList<T_Horarios> getT_Horario() {
        return t_horario;
    }

    public void setId_empleadp(long id_empleado) {
        this.id_empleado = id_empleado;
    }

    public void setDT_RowId(long DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
