package intranet.client.modowifi_visorcheck.modelo.modelosBD;

import intranet.client.modowifi_visorcheck.aplicaciones.Aplicacion;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by pc on 07/07/2018.
 */

public class T_Geocercas extends RealmObject{
    @PrimaryKey
    private long id;
    private long DT_RowId;
    private long id_geo; //funge como id devuelta de la base de datos
    private String descripcion;
    private String longitud;
    private String latitud;
    private String radio;
    private String wifimac; //las macs se agruparan en una sola separadas por comas
    private boolean activo;

    public T_Geocercas() {
    }

    public T_Geocercas(long DT_RowId, long id_geo, String descripcion, String longitud, String latitud, String radio, String wifimac, boolean activo) {
        id= Aplicacion.GeocercaId.incrementAndGet();
        this.DT_RowId=DT_RowId;
        this.id_geo = id_geo;
        this.descripcion = descripcion;
        this.longitud = longitud;
        this.latitud = latitud;
        this.radio = radio;
        this.wifimac = wifimac;
        this.activo = activo;
    }

    public long getDT_RowId() {
        return DT_RowId;
    }

    public void setDT_RowId(long DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public void setId_geo(long id_geo) {
        this.id_geo = id_geo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public void setWifimac(String wifimac) {
        this.wifimac = wifimac;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public long getId() {
        return id;
    }

    public long getId_geo() {
        return id_geo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getLongitud() {
        return longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public String getRadio() {
        return radio;
    }

    public String getWifimac() {
        return wifimac;
    }

    public boolean isActivo() {
        return activo;
    }
}
