package intranet.client.modowifi_visorcheck.actividades;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import intranet.client.modowifi_visorcheck.utilidades.Util;

public class Splash_Screem extends AppCompatActivity{
    private SharedPreferences prefs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs=getSharedPreferences("pruebas_modoWifi", Context.MODE_PRIVATE);
        if(Util.obtenerPreferenciaCondicion(prefs,"entra",false)){
            Intent intent_ModoWifi=new Intent(this,ModoWifi.class);
            intent_ModoWifi.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | intent_ModoWifi.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent_ModoWifi);
        } else{
            Intent intent_Main=new Intent(this,MainActivity.class);
            intent_Main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | intent_Main.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent_Main);
        }
    }
}
