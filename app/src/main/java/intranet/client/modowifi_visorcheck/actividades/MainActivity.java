package intranet.client.modowifi_visorcheck.actividades;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import intranet.client.modowifi_visorcheck.R;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Empleados;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Empresas;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Geocercas;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Horarios;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Logs;
import intranet.client.modowifi_visorcheck.utilidades.Util;
import intranet.client.modowifi_visorcheck.utilidades.UtilidadesRed;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btnAbrir;
    private TextView tvProgreso;
    private String uuid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAbrir=(Button)findViewById(R.id.btnAbrir);
        tvProgreso=(TextView)findViewById(R.id.tvProgreso);
        btnAbrir.setOnClickListener(this);
        uuid = Settings.Secure.getString(this.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==btnAbrir.getId()){
            //iniciarTareaAsincrona
            if(!UtilidadesRed.wifi_habilitado(this)){
                abrirDialog();
            }else {
                TareaAsicrona tareaAsicrona = new TareaAsicrona();
                tareaAsicrona.execute();
            }
        }
    }

    private void abrirDialog() {
        Dialogo_wifi dialogo_wifi=new Dialogo_wifi();
        dialogo_wifi.show(getSupportFragmentManager(),"Dialogo wifi");
    }

    public class TareaAsicrona extends AsyncTask<Void,Integer, Void>{
        private SharedPreferences preferences;
        private Realm realm;
        private T_Empleados t_empleados;
        private boolean tieneInternet=false;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... voids) {
//Realizar insericiones a la base de dato
            realm=Realm.getDefaultInstance();
            preferences=getSharedPreferences("pruebas_modoWifi", Context.MODE_PRIVATE);
            tvProgreso.setText("Sincronizanddo");
                crearGeocerca();
                crearHorario();
                crearEmpresa();
                crearEmpleado();
                crearLog();
                realm.close();
                Util.guardarPreferenciaCondicion(preferences,"entra",true);
                Util.guardarPreferenciaCadena(preferences,"uuid",uuid);
                Util.guardarPreferenciaCondicion(preferences,"status",false);
                return null;
        }

        @Override
        protected void onCancelled() {
            tvProgreso.setText("En  espera");
            super.onCancelled();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            tvProgreso.setText("Sincronizando");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            tvProgreso.setText("Sincronizacion completa");
        }

        private void crearEmpresa(){
            realm.beginTransaction();
            T_Empresas t_empresa=new T_Empresas(
                    836413,"001","SDF161123J56",
                    "SOFT DESIGN FACTORY MEXICO SA DE CV"
            );
            realm.copyToRealm(t_empresa);
            realm.commitTransaction();
        }

        private void crearEmpleado(){
            realm.beginTransaction();

            T_Empleados t_empleados=new T_Empleados(
                    836954,836954,"012","cf5433728aad791a",
                    "JUAN CESAR RUIZ ESPINO",true,1
            );
            realm.copyToRealm(t_empleados);
            T_Empresas t_empresa=realm.where(T_Empresas.class).equalTo("id_empresa",836413).findFirst();
            T_Horarios t_horario=realm.where(T_Horarios.class).equalTo("id_horario",5).findFirst();
            t_empleados.getT_Empresa().add(t_empresa);
            t_empleados.getT_Horario().add(t_horario);
            realm.commitTransaction();
        }
        private void crearGeocerca(){
            realm.beginTransaction();
            T_Geocercas t_geocercas=new T_Geocercas(
                    2,2,"Matriz Oaxaca","17.064238680003847",
                    "96.72453721985221","30",
                    "14:91:82:29:1d:f1,c0:56:27:95:bb:87,c6:4a:00:27:3a:bd",true
            );
            realm.copyToRealm(t_geocercas);
            realm.commitTransaction();
        }
        private void crearHorario(){
            realm.beginTransaction();
            T_Horarios t_horario=new T_Horarios(5,5,"09:00 a 19:30",
                    "09:00","19:30","22:00",
                    "20",true,1);
            realm.copyToRealm(t_horario);
            realm.commitTransaction();
        }
        private void crearLog(){
            realm.beginTransaction();
            T_Logs t_log=new T_Logs(4673,
                    null,"0","0", "2018-07-04 16:11:48",true,1);
            realm.copyToRealm(t_log);
            T_Empleados t_empleado=realm.where(T_Empleados.class).equalTo("id_empleado",4673).findFirst();
            t_log.getEmpleado().add(t_empleado);
            T_Geocercas t_geocerca=realm.where(T_Geocercas.class).equalTo("id_geo",2).findFirst();
            t_log.getGeocerca().add(t_geocerca);
            realm.commitTransaction();
        }
    }
}
