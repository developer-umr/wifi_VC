package intranet.client.modowifi_visorcheck.actividades;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import intranet.client.modowifi_visorcheck.R;
import intranet.client.modowifi_visorcheck.modelo.modelosBD.T_Empleados;
import intranet.client.modowifi_visorcheck.utilidades.Util;
import intranet.client.modowifi_visorcheck.utilidades.UtilidadesRed;
import intranet.client.modowifi_visorcheck.utilidades.UtilidadesTextToSpeech;
import io.realm.Realm;

public class ModoWifi extends AppCompatActivity{
private TextView tvConsola, tvConexion, tvCheck, tvHorario, tvTiempo;
private Realm realm;
private T_Empleados t_empleados;
private String infoRed="";
private SharedPreferences prefs;
private String uuid="";
private String[] horario={"09:00 a 19:30","9:00","19:30","22:00","20"};
private String[] macs={"14:91:82:29:1d:f1","c0:56:27:95:bb:87","c6:4a:00:27:3a:bd","d4:40:f0:2b:94:14"};

private String macAP="";
private boolean estaEnRedEmpresa=false;

private boolean estaEnHorario=true, status=false;
private TextToSpeech textToSpeech;
private UtilidadesTextToSpeech utilTTS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modo_wifi);
        prefs=getSharedPreferences("pruebas_modoWifi",Context.MODE_PRIVATE);
        status=Util.obtenerPreferenciaCondicion(prefs,"status",false);
        tvConsola=(TextView)findViewById(R.id.tvConsola);
        tvConexion=(TextView) findViewById(R.id.tvConexion);
        tvCheck=(TextView) findViewById(R.id.tvLogCheck);
        tvHorario=(TextView) findViewById(R.id.tvHorario);
        tvTiempo=(TextView)findViewById(R.id.tvTiempo);
        uuid=Util.obtenerPreferenciaCadena(prefs,"uuid","");
        utilTTS=new UtilidadesTextToSpeech();
        colocarMensajes();

        WifiManager wifiManager=(WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(UtilidadesRed.wifi_habilitado(this)) {
            if (UtilidadesRed.obtenerTipoConexion(this) == 1) {
                if (horario != null) {
                    if (estaEnHorario) {
                        if(comprobarConexion()){
                            if(status){ //entrada
                                Util.guardarPreferenciaCondicion(prefs,"status",false);
                                tvCheck.setText("Hasta Luego");
                                utilTTS.hastaLuego(this.getApplicationContext());
                            }else{//salida
                                Util.guardarPreferenciaCondicion(prefs,"status",true);
                                tvCheck.setText("Acceso Correcto");
                                utilTTS.accesoCorrecto(this.getApplicationContext());
                            }
                        }
                    } else if (!estaEnHorario && status) { //status=true
                        Util.guardarPreferenciaCondicion(prefs,"status",false);
                        tvCheck.setText("Salida hasta mañana");
                        utilTTS.hastaLuego(this.getApplicationContext());
                    }
                }
            }
        }

    }

    private void obtenerFechaYHoraInternet(){

    }

    private void bloque1(){
        guardarLogBDLocal();
        if(UtilidadesRed.estaEnLinea()){
            enviarLogaNomina();
        } else{

        }
    }

    private void enviarLogaNomina() {
    }

    private void guardarLogBDLocal() {
    }

    private boolean comprobarConexion(){
        macAP=UtilidadesRed.obtenerMAC_AP(this);
        for(int i=0;i<macs.length;i++){
            if(macAP.equals(macs[i])){
                estaEnRedEmpresa=true;
                break;
            }
        }
        return estaEnRedEmpresa;
    }
    private void colocarMensajes(){
        if(status){
            tvCheck.setText("Entrada");
        }else{
            tvCheck.setText("Salida");
        }
        if(UtilidadesRed.wifi_habilitado(this)) {
            tvConsola.setText("wifi habilitado");
        }else {
            tvConsola.setText("wifi esta desactivado");
        }
        if (UtilidadesRed.obtenerTipoConexion(this) == 1) {
            infoRed += "Conexion Tipo:Wifi SSID: " + UtilidadesRed.obtenerNombre_wifi(this) + " " +
                    " MAC AP: " + UtilidadesRed.obtenerMAC_AP(this) +
                    " acceso a internet: " + UtilidadesRed.estaEnLinea() + "";
            tvConexion.setText(infoRed);
        }else {
            infoRed="No esta conectado a una red wifi";
            tvConexion.setText(infoRed);
        }
        if(horario!=null){
            tvHorario.setText("Horario:"+horario[0]+"\nmax salida: "+horario[1]+"\nmax entrada: "+horario[2]);
        }else{
            tvHorario.setText("No tiene horario. Contacte a su administrador");
        }

        SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm:ss");

        org.joda.time.LocalTime localTime= org.joda.time.LocalTime.now();
        LocalDate localDate=LocalDate.now();

        String fechaHoraE=localDate.toString()+horario[1]+":00";
        String fechaHoraS=localDate.toString()+horario[1]+":00";

        Calendar tiempoActual= Calendar.getInstance();

        tvTiempo.setText("Tiempo: "+dateformat.format(tiempoActual.getTime()));
    }
    private void obtenerHorario(){

    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
