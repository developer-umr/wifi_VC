package intranet.client.modowifi_visorcheck.actividades;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;

import intranet.client.modowifi_visorcheck.utilidades.UtilidadesRed;

public class Dialogo_wifi extends AppCompatDialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Wifi").setMessage("Wifi deshabilitado").setPositiveButton("Habilitar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               UtilidadesRed.encenderWifi(getActivity().getApplicationContext());
            }
        });
        return builder.create();
    }
}
